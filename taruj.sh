#!/bin/bash

tar -cvf "number1.tar" "number.sh";
for i in {2..10000};
do
	tar -cvf "number${i}.tar" "number$(( i-1 )).tar";
	rm "number$(( i-1 )).tar";
done
