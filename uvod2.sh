#!/bin/bash
vslp=0.1
lslp=0.005
bslp=1
mslp=2

input="./linux.txt"

s="\e[40;5;248m"
c="\e[40;5;232m"
b="\e[40;5;7m"
n="\e[40;5;214m"
z="\e[40;5;226m"

islnd1=" \ / \ / / \ \ \ / / / \ \ / \ / / \ \ \ / / / \ \ / \ / / \ \ \ / / / \ "

echo -e "\tAROS lab. vezbe"| /usr/bin/figlet


sleep $bslp

                                                                        
                                                                        
#                         Predavaci:                                     
#                 Maja Miljanic    : miljanic.maja0@gmail.com            
#                 Mirko Nikic      : 4370557@gmail.com                   
#                                                                        
#	           \/\//\\\///\\/\//\\\///\\/\//\\\///\\/\//\
#	             \\\//\/\\///\\\//\/\\///\\\//\/\\///\/
#                     \/    \/\\//\/    \/\\//\/    \/\\/
#                           \/          \/          \/
#
#		
#
#
#			< Tema kursa: Linux >
# 			-------------------
#               	       \
#    				\
#			        .--.
#			       |o_o |
#       		       |:_/ |
#      			      //   \ \
#     			     (|     | )
#    			    /'\_   _/`\
# 			    \___)=(___/
#
#
#
#
#

bckgr_line() {
for j in {1..6}; do for i in {16..21} {21..16} ; do echo -en "\e[48;5;${i}m \e[0m"; sleep $lslp; done; done;
echo
}

blck_line() {
for j in {1..6};do for i in {16..21} {21..16} ; do echo -en "\e[48;5;0m \e[0m"; sleep $lslp ; done; done;
echo
}

line1() {
#leva granica
for i in {16..21} ; do echo -en "\e[48;5;0m \e[0m"; sleep $lslp ; done ;

for i in {21..16} ; do echo -en "\e[48;5;${i}m \e[0m"; sleep $lslp ; done ;

for i in {16..21} {21..16} ; do echo -en "\e[38;5;${i}m#\e[0m" ; done ;
for i in {16..21} {21..16} ; do echo -en "\e[38;5;${i}m#\e[0m" ; done ;
for i in {16..21} {21..16} ; do echo -en "\e[38;5;${i}m#\e[0m" ; done ;
for i in {16..21} {21..16} ; do echo -en "\e[38;5;${i}m#\e[0m" ; done ;

for i in {16..21} ; do echo -en "\e[48;5;${i}m \e[0m"; sleep $lslp ; done ;

#desna granica
for i in {21..16} ; do echo -en "\e[48;5;0m \e[0m"; sleep $lslp ; done ;
echo
}

line2() {
for i in {16..21} ; do echo -en "\e[48;5;0m \e[0m"; sleep $lslp ; done ;
for i in {21..16} ; do echo -en "\e[48;5;${i}m \e[0m"; sleep $lslp ; done ;

for i in {16..21} {21..16} ; do echo -en "\e[48;5;${i}m#\e[0m" ; done ;
for i in {16..21} {21..16} ; do echo -en "\e[48;5;${i}m#\e[0m" ; done ;
for i in {16..21} {21..16} ; do echo -en "\e[48;5;${i}m#\e[0m" ; done ;
for i in {16..21} {21..16} ; do echo -en "\e[48;5;${i}m#\e[0m" ; done ;

for i in {16..21} ; do echo -en "\e[48;5;${i}m \e[0m"; sleep $lslp ; done ;
for i in {21..16} ; do echo -en "\e[48;5;0m \e[0m"; sleep $lslp ; done ;
echo
}

#                       < Tema kursa: Linux >
#                       -------------------
#                              \
#                               \
#                               .--.
#                              |o_o |
#                              |:_/ |
#                             //   \ \
#                            (|     | )
#                           /'\_   _/`\
#                           \___)=(___/


pingvin() {
while IFS= read -r line
do 
	echo -e "                  $line"
done < "$input"
echo
}

predavaci_line(){
echo -ne "\e[1m"
for i in {16..21} {21..16} ; do echo -en "\e[48;5;${i}m \e[0m"; sleep $lslp ; done ;
for i in {16..21} {21..16} ; do echo -en "\e[48;5;${i}m \e[0m"; sleep $lslp ; done ;
echo -en "\e[48;5;27m Predavaci: "
for i in {16..21} {21..16} ; do echo -en "\e[48;5;${i}m \e[0m"; sleep $lslp ; done ;
for i in {16..21} {21..16} ; do echo -en "\e[48;5;${i}m \e[0m"; sleep $lslp ; done ;
for i in {16..21} {21..16} ; do echo -en "\e[48;5;${i}m \e[0m"; sleep $lslp ; done ;
echo

for i in {16..21} {21..16} ; do echo -en "\e[48;5;${i}m \e[0m"; sleep $lslp ; done ;
echo -en "\e[48;5;27m     Maja Miljanic    : miljanic.maja0@gmail.com"
for i in {16..21} {21..16} ; do echo -en "\e[48;5;${i}m \e[0m"; sleep $lslp ; done ;
echo

for i in {16..21} {21..16} ; do echo -en "\e[48;5;${i}m \e[0m"; sleep $lslp ; done ;
echo -en "\e[48;5;27m     Mirko Nikic      : 4370557@gmail.com       "
for i in {16..21} {21..16} ; do echo -en "\e[48;5;${i}m \e[0m"; sleep $lslp ; done ;
echo -e "\e[0m"
}

bckgr_line
bckgr_line
predavaci_line
bckgr_line
blck_line
echo -e "\e[48;5;0m            \e[1m\e[5m cas 2; Tema: Principi UNIX-a i rad u terminalu\e[48;5;0m             \e[0m"
line1
line1

pingvin

line1
line1
blck_line
blck_line

sleep $mslp
printf "\tProjekat"| /usr/bin/figlet
printf "\n"
printf "\n"
sleep $lslp
for i in {'Z','a',' ','p','r','o','j','e','k','a','t',' ','s','e',' ','m','o','z','e','t','e',' ','p','r','i','j','a','v','i','t','i',' ','d','o',' ','k','r','a','j','a',' ','s','e','m','e','s','t','r','a','.'}; do echo -en "${i}"; sleep $vslp ; done ;
printf "\n"
for i in {'T','h','e',' ','L','i','t','t','l','e',' ','B','o','o','k',' ','o','f',' ','S','e','m','a','p','h','o','r','e','s'}; do echo -en "${i}"; sleep $vslp ; done ;
printf "\n"
for i in {'P','r','o','j','e','k','a','t',' ','m','o','z','e','t','e',' ','b','r','a','n','i','t','i',' ','d','o',' ','k','r','a','j','a',' ','g','o','d','i','n','e''.'}; do echo -en "${i}"; sleep $vslp ; done ;
printf "\n"
printf "\n"
sleep $mslp
blck_line
line1
blck_line
printf "\tKonsultacije"| /usr/bin/figlet
for i in {'M','A','J','A',':''\t','S','r','e','d','a',':','11','-','14','h'}; do echo -en "${i}"; sleep $vslp ; done ;
printf "\n"
for i in {'M','I','R','K','O',':''\t','P','e','t','a','k',':','14','-','17','h'}; do echo -en "${i}"; sleep $vslp ; done ;
printf "\n"
printf "\n"

