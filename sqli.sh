#!/bin/bash
vslp=0.1
lslp=0.005
bslp=1
mslp=2



s="\e[40;5;248m"
c="\e[40;5;232m"
b="\e[40;5;7m"
n="\e[40;5;214m"
z="\e[40;5;226m"

islnd1=" \ / \ / / \ \ \ / / / \ \ / \ / / \ \ \ / / / \ \ / \ / / \ \ \ / / / \ "




sleep $bslp


bckgr_line() {
for j in {1..6}; do for i in {16..24} {24..16} ; do echo -en "\e[48;5;${i}m \e[0m"; sleep $lslp; done; done;
echo
}

blck_line() {
for j in {1..6};do for i in {16..24} {24..16} ; do echo -en "\e[48;5;0m \e[0m"; sleep $lslp ; done; done;
echo
}

line1() {

for i in {16..24} ; do echo -en "\e[48;5;${i}m \e[0m"; sleep $lslp ; done ;
for i in {24..16} ; do echo -en "\e[48;5;${i}m \e[0m"; sleep $lslp ; done ;

for i in {16..24} {24..16} ; do echo -en "\e[38;5;${i}m#\e[0m" ; done ;
for i in {16..24} {24..16} ; do echo -en "\e[38;5;${i}m#\e[0m" ; done ;
for i in {16..24} {24..16} ; do echo -en "\e[38;5;${i}m#\e[0m" ; done ;
for i in {16..24} {24..16} ; do echo -en "\e[38;5;${i}m#\e[0m" ; done ;

for i in {16..24} ; do echo -en "\e[48;5;${i}m \e[0m"; sleep $lslp ; done ;
for i in {24..16} ; do echo -en "\e[48;5;${i}m \e[0m"; sleep $lslp ; done ;
echo
}

predmet_line(){
echo -ne "\e[1m"
for i in {16..24} {24..16} ; do echo -en "\e[48;5;${i}m \e[0m"; sleep $lslp ; done ;
for i in {16..24} {24..16} ; do echo -en "\e[48;5;${i}m \e[0m"; sleep $lslp ; done ;
echo -en "\e[48;5;27m Razvoj bezbednog softvera"
for i in {24..24} {24..16} ; do echo -en "\e[48;5;${i}m \e[0m"; sleep $lslp ; done ;
for i in {16..24} {24..16} ; do echo -en "\e[48;5;${i}m \e[0m"; sleep $lslp ; done ;
for i in {16..24} {24..16} ; do echo -en "\e[48;5;${i}m \e[0m"; sleep $lslp ; done ;

echo -e "\e[0m"
}

bckgr_line
bckgr_line
predmet_line
bckgr_line
bckgr_line
blck_line
echo -e "\t\t\t\t\t\tSQL injection"| /usr/bin/figlet -f slant | /usr/games/lolcat -a
blck_line
bckgr_line
bckgr_line

echo -e "\e[48;5;0m            \e[1m\e[5m \t\t\t\t      SADRZAJ           \e[0m"
line1
echo -e "\t\t\t\t\t1.Detekcija ranjivosti"
line1
echo -e "\t\t\t\t\t2.Poznati napadi"
line1
echo -e "\t\t\t\t\t3.Vrste napada i primeri"
line1
echo -e "\t\t\t\t\t4.Prevencija SQLi"
bckgr_line
bckgr_line
blck_line
blck_line
read -p "Press enter to continue..."
blck_line
echo -e "\t\t  1.Detekcija ranjivosti"| /usr/bin/figlet -f slant | /usr/games/lolcat
blck_line
bckgr_line
bckgr_line
blck_line
sleep $mslp
printf "\t"
for i in {'R','a','i','n',' ','F','o','r','e','s','t',' ','P','u','p','p','y'}; do echo -en "${i}"; sleep $vslp ; done ;
printf "\n"
printf "\t"
for i in {'1','9','9','8','.',' ','P','h','r','a','c','k',' ','m','a','g','a','z','i','n','e'}; do echo -en "${i}"; sleep $vslp ; done ;
sleep $mslp
printf "\n"
printf "\t"
for i in {'I','I','S',' ','-','M','S',' ','S','Q','L',' ','s','e','r','v','e','r',' ','-','.','i','d','c',' ','f','i','l','e'}; do echo -en "${i}"; sleep $vslp ; done ;
printf "\n"
printf "\n"
printf "\t"
for i in {'N','a','g','o','m','i','l','a','v','a','n','j','e',' ','u','p','i','t','a'}; do echo -en "${i}"; sleep $vslp ; done ;
printf "\n"
echo -e "\n\n\tSELECT * FROM table WHERE x=1 SELECT * FROM table WHERE y=5.\n\n"| /usr/bin/boxes | /usr/games/lolcat
printf "\n"
sleep $mslp
printf "\n"
printf "\t"
for i in {'P','i','g','g','y','b','a','c','k'}; do echo -en "${i}"; sleep $vslp ; done ;
printf "\n"
echo -e "\n\n\tupit u .idc fajlu: SELECT * FROM table WHERE x=%%criteria from webpage user%%\n\n\tinput: 1 SELECT * FROM sysobjects\n\n\tSELECT * FROM table WHERE x=1 SELECT * FROM sysobjects\n\n"| /usr/bin/boxes
printf "\n"
printf "\t"
for i in {'K','o','r','i','s','c','e','n','j','e',' ','k','o','m','e','n','t','a','r','a'}; do echo -en "${i}"; sleep $vslp ; done ;
printf "\n"
echo -e "\n\n\tupit u .idc fajlu: SELECT * FROM phonetable WHERE NAME='%name%'\n\n\trequest: phone.idc?name=rfp' select * from table2--\n\n\tSELECT * FROM phonetable WHERE name='rfp' select * from table2--'\n\n"| /usr/bin/boxes
blck_line
bckgr_line
bckgr_line
blck_line
read -p "Press enter to continue..."
blck_line
echo -e "\t\t  2.Poznati napadi"| /usr/bin/figlet -f slant | /usr/games/lolcat
blck_line
bckgr_line
bckgr_line
blck_line
echo -e "\t\t\t\t\t1.Rockyou"
line1
echo -e "\t\t\t\t\t2.7Eleven"
line1
echo -e "\t\t\t\t\t3.Sony Music Japan, Sony Pictures"
line1
echo -e "\t\t\t\t\t4.Equifax"
line1
printf "\n"
echo -e "\e[48;5;0m            \e[1m\e[5m \t\t\t     SQLi Hall Of Shame           \e[0m"
blck_line
bckgr_line
bckgr_line
blck_line
read -p "Press enter to continue..."
blck_line
echo -e "\t\t  3.Vrste napada"| /usr/bin/figlet -f slant | /usr/games/lolcat
blck_line
bckgr_line
bckgr_line
echo -e "\t\t\t\t\t1.Napad zasnovan na tautologiji"
line1
echo -e "\t\t\t\t\t2.Union query napad"
line1
echo -e "\t\t\t\t\t3.Napad sa logicki neispravnim upitima"
line1
echo -e "\t\t\t\t\t4.Piggyback napad"
line1
echo -e "\t\t\t\t\t5.Inference napad"
bckgr_line
bckgr_line
blck_line
jp2a dvwa.jpeg --colors --width=120
blck_line
read -p "Press enter to continue..."
blck_line
echo -e "\t\t  3.Prevencija SQLi"| /usr/bin/figlet -f slant | /usr/games/lolcat
blck_line
bckgr_line
bckgr_line
echo -e "\t\t\t\t\t1.Sanitizacija inputa"
line1
echo -e "\t\t\t\t\t2.Parametrizovani upiti"
line1
echo -e "\t\t\t\t\t3.Ostale mere zastite"
bckgr_line
bckgr_line
printf "\n"
echo "Hvala na paznji!" | /usr/games/lolcat -a -d 300
#
